package com.xyz.controller;

import com.xyz.URLMapping;
import com.xyz.model.MachineSecurity;
import com.xyz.service.MachineSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping(value = URLMapping.MachineSecurity.BASE_URL)
public class MachineSecurityController {
  @Autowired MachineSecurityService machineSecurityService;

  @RequestMapping(method = RequestMethod.GET)
  public List<MachineSecurity> findAll() {
    return machineSecurityService.findAll();
  }

  @RequestMapping(value = URLMapping.ID_URL, method = RequestMethod.GET)
  public ResponseEntity findById(@PathVariable Long id) {
    Optional<MachineSecurity> optionalMachineSecurity = machineSecurityService.findById(id);

    if (!optionalMachineSecurity.isPresent()) {
      return new ResponseEntity<>(
          "No existe seguridad de máquina con ID " + id, HttpStatus.BAD_REQUEST);
    }

    return new ResponseEntity<>(optionalMachineSecurity.get(), HttpStatus.OK);
  }

  @RequestMapping(value = URLMapping.MachineSecurity.OPEN, method = RequestMethod.POST)
  public ResponseEntity open(@RequestBody Map<String, Object> payload) {
    MachineSecurity machineSecurity =
        machineSecurityService.findById(((Number) payload.get("id")).longValue()).get();

    if (machineSecurity.isBlocked()) {
      return new ResponseEntity<>("La máquina está bloqueada", HttpStatus.BAD_REQUEST);
    } else if (!machineSecurity.getCode().equals(payload.get("code"))) {
      machineSecurity.setAttempts(machineSecurity.getAttempts() + 1);
      if (machineSecurity.getAttempts() > 2) {
        machineSecurity.setBlocked(true);
        machineSecurityService.save(machineSecurity);
        return new ResponseEntity<>("Se ha bloqueado la máquina", HttpStatus.BAD_REQUEST);
      } else {
        machineSecurityService.save(machineSecurity);
        return new ResponseEntity<>("Código inválido", HttpStatus.BAD_REQUEST);
      }
    }

    machineSecurity.setAttempts(0);
    machineSecurityService.save(machineSecurity);

    return new ResponseEntity<>(HttpStatus.OK);
  }

  @RequestMapping(value = URLMapping.MachineSecurity.UNLOCK, method = RequestMethod.POST)
  public ResponseEntity unlock(@RequestBody Map<String, Object> payload) {
    MachineSecurity machineSecurity =
        machineSecurityService.findById(((Number) payload.get("id")).longValue()).get();

    machineSecurity.setAttempts(0);
    machineSecurity.setBlocked(false);
    machineSecurityService.save(machineSecurity);

    return new ResponseEntity<>(HttpStatus.OK);
  }
}
