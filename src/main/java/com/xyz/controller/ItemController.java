package com.xyz.controller;

import com.xyz.URLMapping;
import com.xyz.dto.ItemDTO;
import com.xyz.model.Item;
import com.xyz.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = URLMapping.Item.BASE_URL)
public class ItemController {
  @Autowired ItemService itemService;

  @RequestMapping(method = RequestMethod.GET)
  public List<ItemDTO> findAll() {
    return ItemDTO.itemsToDTOList(itemService.findAll());
  }

  @RequestMapping(value = URLMapping.ID_URL, method = RequestMethod.GET)
  public ResponseEntity findById(@PathVariable Long id) {
    Optional<Item> optionalItem = itemService.findById(id);

    if (!optionalItem.isPresent()) {
      return new ResponseEntity<>("No existe item con ID " + id, HttpStatus.BAD_REQUEST);
    }

    return new ResponseEntity<>(new ItemDTO(optionalItem.get()), HttpStatus.OK);
  }
}
