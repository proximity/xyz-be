package com.xyz.controller;

import com.xyz.URLMapping;
import com.xyz.model.Alert;
import com.xyz.service.AlertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = URLMapping.Alert.BASE_URL)
public class AlertController {
  @Autowired AlertService alertService;

  @RequestMapping(method = RequestMethod.GET)
  public List<Alert> findAll() {
    return alertService.findAll();
  }

  @RequestMapping(value = URLMapping.ID_URL, method = RequestMethod.GET)
  public ResponseEntity findById(@PathVariable Long id) {
    Optional<Alert> optionalAlert = alertService.findById(id);

    if (!optionalAlert.isPresent()) {
      return new ResponseEntity<>("No existe alerta con ID " + id, HttpStatus.BAD_REQUEST);
    }

    return new ResponseEntity<>(optionalAlert.get(), HttpStatus.OK);
  }
}
