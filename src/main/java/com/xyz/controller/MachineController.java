package com.xyz.controller;

import com.xyz.URLMapping;
import com.xyz.dto.MachineDTO;
import com.xyz.dto.SaleDTO;
import com.xyz.model.Alert;
import com.xyz.model.Company;
import com.xyz.model.Machine;
import com.xyz.model.Sale;
import com.xyz.service.*;
import com.xyz.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = URLMapping.Machine.BASE_URL)
public class MachineController {
  @Autowired MachineService machineService;
  @Autowired SoldItemService soldItemService;
  @Autowired SaleService saleService;
  @Autowired CompanyService companyService;
  @Autowired AlertService alertService;

  @RequestMapping(method = RequestMethod.GET)
  public List<MachineDTO> findAll() {
    return MachineDTO.machinesToDTOList(machineService.findAll());
  }

  @RequestMapping(value = URLMapping.ID_URL, method = RequestMethod.GET)
  public ResponseEntity findById(@PathVariable Long id) {
    Optional<Machine> optionalMachine = machineService.findById(id);

    if (!optionalMachine.isPresent()) {
      return new ResponseEntity<>("No existe máquina con ID " + id, HttpStatus.BAD_REQUEST);
    }

    return new ResponseEntity<>(new MachineDTO(optionalMachine.get()), HttpStatus.OK);
  }

  @RequestMapping(value = URLMapping.Machine.TOTAL_MONEY_EARNED, method = RequestMethod.GET)
  public ResponseEntity totalMoneyEarned(@PathVariable Long id) {
    int totalMoneyEarned =
        soldItemService.findSoldItemsByMachineId(id).stream()
            .mapToInt(soldItem -> soldItem.getItem().getPrice() * soldItem.getQuantity())
            .sum();

    return new ResponseEntity<>(Util.numberToCurrency(totalMoneyEarned), HttpStatus.OK);
  }

  @RequestMapping(value = URLMapping.Machine.NUMBER_OF_TRANSACTIONS, method = RequestMethod.GET)
  public ResponseEntity numberOfTransactions(@RequestParam Long id, @RequestParam String date) {
    return new ResponseEntity<>(
        saleService
            .findSalesByMachineIdStartDateAndEndDate(
                id, Util.parseDate(date), Util.addDaysToDate(date, 1))
            .size(),
        HttpStatus.OK);
  }

  @RequestMapping(value = URLMapping.Machine.SALES, method = RequestMethod.GET)
  public List<SaleDTO> sales(@PathVariable Long id) {
    List<SaleDTO> dtoList = new ArrayList<>();

    List<Sale> saleList = saleService.findSalesByMachineId(id);
    saleList.forEach(
        sale ->
            dtoList.add(new SaleDTO(sale, soldItemService.findSoldItemsBySaleId(sale.getId()))));

    return dtoList;
  }

  @RequestMapping(value = URLMapping.Machine.WITHDRAW_MONEY, method = RequestMethod.GET)
  public ResponseEntity withdrawMoney(@PathVariable Long id) {
    Machine machine = machineService.findById(id).get();

    if (machine.getMoney() > 500) {
      Company company = companyService.findById(machine.getCompany().getId()).get();
      company.setMoney(company.getMoney() + (machine.getMoney() - 500));
      company.setVersion(company.getVersion() + 1);
      company.setUpdated(new Date());
      companyService.save(company);

      machine.setMoney(500);
      machine.setVersion(machine.getVersion() + 1);
      machine.setUpdated(new Date());
      machineService.save(machine);
    }

    Alert alert = alertService.findAlertByMachineId(machine.getId());
    if (alert != null) {
      alertService.deleteById(alert.getId());
    }

    return new ResponseEntity<>(HttpStatus.OK);
  }
}
