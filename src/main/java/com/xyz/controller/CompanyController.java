package com.xyz.controller;

import com.xyz.URLMapping;
import com.xyz.dto.CompanyDTO;
import com.xyz.model.Company;
import com.xyz.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = URLMapping.Company.BASE_URL)
public class CompanyController {
  @Autowired CompanyService companyService;

  @RequestMapping(method = RequestMethod.GET)
  public List<CompanyDTO> findAll() {
    return CompanyDTO.companiesToDTOList(companyService.findAll());
  }

  @RequestMapping(value = URLMapping.ID_URL, method = RequestMethod.GET)
  public ResponseEntity findById(@PathVariable Long id) {
    Optional<Company> optionalCompany = companyService.findById(id);

    if (!optionalCompany.isPresent()) {
      return new ResponseEntity<>("No existe compañía con ID " + id, HttpStatus.BAD_REQUEST);
    }

    return new ResponseEntity<>(new CompanyDTO(optionalCompany.get()), HttpStatus.OK);
  }
}
