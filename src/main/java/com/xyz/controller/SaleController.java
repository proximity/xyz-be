package com.xyz.controller;

import com.xyz.URLMapping;
import com.xyz.dto.SaleDTO;
import com.xyz.model.Alert;
import com.xyz.model.Machine;
import com.xyz.model.Sale;
import com.xyz.model.SoldItem;
import com.xyz.request.SaleRequest;
import com.xyz.service.*;
import com.xyz.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping(value = URLMapping.Sale.BASE_URL)
public class SaleController {
  @Autowired SaleService saleService;
  @Autowired MachineService machineService;
  @Autowired SoldItemService soldItemService;
  @Autowired ItemService itemService;
  @Autowired AlertService alertService;

  @RequestMapping(method = RequestMethod.GET)
  public List<SaleDTO> findAll() {
    List<SaleDTO> dtoList = new ArrayList<>();

    List<Sale> saleList = saleService.findAll();
    saleList.forEach(
        sale ->
            dtoList.add(new SaleDTO(sale, soldItemService.findSoldItemsBySaleId(sale.getId()))));

    return dtoList;
  }

  @RequestMapping(value = URLMapping.ID_URL, method = RequestMethod.GET)
  public ResponseEntity findById(@PathVariable Long id) {
    Optional<Sale> optionalSale = saleService.findById(id);

    if (!optionalSale.isPresent()) {
      return new ResponseEntity<>("No existe venta con ID " + id, HttpStatus.BAD_REQUEST);
    }

    return new ResponseEntity<>(
        new SaleDTO(optionalSale.get(), soldItemService.findSoldItemsBySaleId(id)), HttpStatus.OK);
  }

  @RequestMapping(method = RequestMethod.POST)
  public ResponseEntity save(@RequestBody SaleRequest saleRequest) {
    if (!saleRequest.getItems().isEmpty()) {
      if (machineService.findById(saleRequest.getMachineId()).get().getModel().equals("XYZ1")
          && !saleRequest.getPaymentMethod().equals("Efectivo")) {
        return new ResponseEntity<>("No se aceptan tarjetas", HttpStatus.BAD_REQUEST);
      }

      Map<String, Object> result = new HashMap<>();
      result.put("message", "¡Tu compra ha sido realizada con éxito!");
      result.put("alert", "");

      int total =
          (int) Math.round((Double.parseDouble(saleRequest.getTotal().replace("$", "")) * 100));

      if (saleRequest.getPaymentMethod().equals("Efectivo")) {
        int payment =
            (int) Math.round((Double.parseDouble(saleRequest.getPayment().replace("$", "")) * 100));

        if (payment < total) {
          return new ResponseEntity<>("Pago insuficiente", HttpStatus.BAD_REQUEST);
        }

        result.put("change", Util.numberToCurrency(payment - total));
        result.put("coins", Util.minimumCoinsNumber(payment - total));
      } else if (!Util.simulateConnectionToThirdPartySystem()) {
        return new ResponseEntity<>("Se ha rechazado la tarjeta", HttpStatus.BAD_REQUEST);
      }

      registerSale(saleRequest);

      checkMoneyOnMachine(saleRequest.getMachineId(), total, result);

      return new ResponseEntity<>(result, HttpStatus.OK);
    }

    return new ResponseEntity<>("No se recibieron items", HttpStatus.BAD_REQUEST);
  }

  public void registerSale(SaleRequest saleRequest) {
    Sale sale = new Sale();
    sale.setMachine(machineService.findById(saleRequest.getMachineId()).get());
    sale.setPaymentMethod(saleRequest.getPaymentMethod());
    sale.setDate(new Date());
    saleService.save(sale);

    saleRequest
        .getItems()
        .forEach(
            item -> {
              SoldItem soldItem = new SoldItem();
              soldItem.setSale(sale);
              soldItem.setItem(itemService.findById(((Number) item.get("item")).longValue()).get());
              soldItem.setQuantity(((Number) item.get("quantity")).intValue());
              soldItemService.save(soldItem);
            });
  }

  public void checkMoneyOnMachine(Long machineId, int total, Map<String, Object> result) {
    Machine machine = machineService.findById(machineId).get();
    machine.setMoney(machine.getMoney() + total);
    machine.setVersion(machine.getVersion() + 1);
    machine.setUpdated(new Date());
    machineService.save(machine);

    if (machine.getMoney() >= 10000) {
      Alert alert = alertService.findAlertByMachineId(machineId);

      if (alert == null) {
        alert = new Alert();
        alert.setMachine(machine);
        alert.setMessage("La máquina " + machine.getId() + " está lista para retirar el dinero");
        alert.setDate(new Date());
        alertService.save(alert);
      }

      Map<String, Object> alertResult = new HashMap<>();
      alertResult.put("id", alert.getId());
      alertResult.put("id_machine", alert.getMachine().getId());
      alertResult.put("message", alert.getMessage());
      alertResult.put("date", alert.getDate());
      result.put("alert", alertResult);
    }
  }
}
