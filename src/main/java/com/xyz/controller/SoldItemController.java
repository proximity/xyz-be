package com.xyz.controller;

import com.xyz.URLMapping;
import com.xyz.dto.SoldItemDTO;
import com.xyz.model.SoldItem;
import com.xyz.service.SoldItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = URLMapping.SoldItem.BASE_URL)
public class SoldItemController {
  @Autowired SoldItemService soldItemService;

  @RequestMapping(method = RequestMethod.GET)
  public List<SoldItemDTO> findAll() {
    return SoldItemDTO.soldItemsToDTOList(soldItemService.findAll());
  }

  @RequestMapping(value = URLMapping.ID_URL, method = RequestMethod.GET)
  public ResponseEntity findById(@PathVariable Long id) {
    Optional<SoldItem> optionalSoldItem = soldItemService.findById(id);

    if (!optionalSoldItem.isPresent()) {
      return new ResponseEntity<>(
          "No existe artículo vendido con ID " + id, HttpStatus.BAD_REQUEST);
    }

    return new ResponseEntity<>(new SoldItemDTO(optionalSoldItem.get()), HttpStatus.OK);
  }
}
