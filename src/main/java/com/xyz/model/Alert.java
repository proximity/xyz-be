package com.xyz.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
public class Alert {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id")
  private Long id;

  @ManyToOne
  @JoinColumn(name = "id_machine")
  private Machine machine;

  @Column(name = "message")
  private String message;

  @Column(name = "date")
  private Date date;
}
