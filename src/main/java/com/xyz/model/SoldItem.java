package com.xyz.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class SoldItem {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id")
  private Long id;

  @ManyToOne
  @JoinColumn(name = "id_sale")
  private Sale sale;

  @ManyToOne
  @JoinColumn(name = "id_item")
  private Item item;

  @Column(name = "quantity")
  private int quantity;
}
