package com.xyz.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
public class Machine {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id")
  private Long id;

  @ManyToOne
  @JoinColumn(name = "id_company")
  private Company company;

  @Column(name = "model")
  private String model;

  @Column(name = "money")
  private int money;

  @Column(name = "version")
  private int version;

  @Column(name = "created")
  private Date created;

  @Column(name = "updated")
  private Date updated;
}
