package com.xyz.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
public class Company {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id")
  private Long id;

  @Column(name = "name")
  private String name;

  @Column(name = "money")
  private int money;

  @Column(name = "version")
  private int version;

  @Column(name = "created")
  private Date created;

  @Column(name = "updated")
  private Date updated;
}
