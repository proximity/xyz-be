package com.xyz.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
public class Sale {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id")
  private Long id;

  @ManyToOne
  @JoinColumn(name = "id_machine")
  private Machine machine;

  @Column(name = "payment_method")
  private String paymentMethod;

  @Column(name = "date")
  private Date date;
}
