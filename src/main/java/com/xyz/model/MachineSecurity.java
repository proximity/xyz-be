package com.xyz.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class MachineSecurity {
  @Id
  @Column(name = "id_machine")
  private Long machineId;

  @Column(name = "code")
  private String code;

  @Column(name = "attempts")
  private int attempts;

  @Column(name = "blocked")
  private boolean blocked;
}
