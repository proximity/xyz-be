package com.xyz.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
public class Item {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "code")
  private Long code;

  @Column(name = "item")
  private String item;

  @Column(name = "price")
  private int price;

  @Column(name = "version")
  private int version;

  @Column(name = "created")
  private Date created;

  @Column(name = "updated")
  private Date updated;
}
