package com.xyz.util;

import lombok.SneakyThrows;

import java.text.SimpleDateFormat;
import java.util.*;

public class Util {
  public static String numberToCurrency(int number) {
    StringBuilder value = new StringBuilder();

    int dollars = number / 100;
    int pennies = number % 100;

    value.append("$");

    if (dollars > 0) {
      value.append(dollars);
    } else {
      if (pennies >= 0) {
        value.append("0.").append(pennies);
      }

      return value.toString();
    }
    if (pennies > 0) {
      value.append(".").append(pennies);
    }

    return value.toString();
  }

  public static String minimumCoinsNumber(int number) {
    int[] coinArray = new int[] {50, 25, 10, 5};

    Map<Integer, Integer> integerMap = new HashMap<>();

    for (int value : coinArray) {
      while (number >= value) {
        number -= value;
        if (integerMap.containsKey(value)) {
          integerMap.put(value, integerMap.get(value) + 1);
        } else {
          integerMap.put(value, 1);
        }
      }
    }

    StringBuilder value = new StringBuilder();

    integerMap.entrySet().stream()
        .sorted(Collections.reverseOrder(Map.Entry.comparingByKey()))
        .forEach(
            entry ->
                value
                    .append(entry.getValue())
                    .append(entry.getValue() > 1 ? " monedas de " : " moneda de ")
                    .append(entry.getKey())
                    .append(", "));

    return value.toString().length() > 0
        ? value.toString().substring(0, value.toString().length() - 2)
        : "0";
  }

  public static boolean simulateConnectionToThirdPartySystem() {
    Random random = new Random();
    return random.nextBoolean();
  }

  @SneakyThrows
  public static Date parseDate(String date) {
    return new SimpleDateFormat("yyyy-MM-dd").parse(date);
  }

  @SneakyThrows
  public static Date addDaysToDate(String date, int days) {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    Calendar calendar = Calendar.getInstance();
    calendar.setTime(simpleDateFormat.parse(date));
    calendar.add(Calendar.DAY_OF_MONTH, days);

    return parseDate(simpleDateFormat.format(calendar.getTime()));
  }
}
