package com.xyz.request;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class SaleRequest {
  private Long machineId;
  private String total;
  private String paymentMethod;
  private String payment;
  private List<Map<String, Object>> items;
}
