package com.xyz.dto;

import com.xyz.model.SoldItem;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class SoldItemDTO {
  private long id;
  private ItemDTO item;
  private int quantity;
  private Map<String, Object> sale;

  public SoldItemDTO(SoldItem soldItem) {
    this.id = soldItem.getId();
    this.item = new ItemDTO(soldItem.getItem());
    this.quantity = soldItem.getQuantity();
    this.sale = new HashMap<>();
    this.sale.put("id", soldItem.getSale().getId());
    this.sale.put("date", soldItem.getSale().getDate());
    this.sale.put("paymentMethod", soldItem.getSale().getPaymentMethod());
    this.sale.put("machine", soldItem.getSale().getMachine().getId());
  }

  public static List<SoldItemDTO> soldItemsToDTOList(List<SoldItem> soldItemList) {
    List<SoldItemDTO> dtoList = new ArrayList<>();

    soldItemList.forEach(soldItem -> dtoList.add(new SoldItemDTO(soldItem)));

    return dtoList;
  }
}
