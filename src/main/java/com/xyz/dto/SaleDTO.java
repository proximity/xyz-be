package com.xyz.dto;

import com.xyz.model.Sale;
import com.xyz.model.SoldItem;
import com.xyz.util.Util;
import lombok.Data;

import java.util.*;

@Data
public class SaleDTO {
  private long id;
  private Date date;
  private String paymentMethod;
  private MachineDTO machine;
  private List<Map<String, Object>> soldItems;
  private String total;

  public SaleDTO(Sale sale, List<SoldItem> soldItemList) {
    this.id = sale.getId();
    this.date = sale.getDate();
    this.paymentMethod = sale.getPaymentMethod();
    this.machine = new MachineDTO(sale.getMachine());
    this.soldItems = new ArrayList<>();
    soldItemList.forEach(
        soldItem -> {
          Map<String, Object> itemMap = new HashMap<>();

          itemMap.put("code", soldItem.getItem().getCode());
          itemMap.put("item", soldItem.getItem().getItem());
          itemMap.put("price", Util.numberToCurrency(soldItem.getItem().getPrice()));
          itemMap.put("quantity", soldItem.getQuantity());

          this.soldItems.add(itemMap);
        });
    this.total = calculateTotal(soldItemList);
  }

  public String calculateTotal(List<SoldItem> soldItemList) {
    int total = 0;

    for (SoldItem soldItem : soldItemList) {
      total += soldItem.getItem().getPrice() * soldItem.getQuantity();
    }

    return Util.numberToCurrency(total);
  }
}
