package com.xyz.dto;

import com.xyz.model.Machine;
import com.xyz.util.Util;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class MachineDTO {
  private long id;
  private String model;
  private String money;
  private Map<String, Object> company;

  public MachineDTO(Machine machine) {
    this.id = machine.getId();
    this.model = machine.getModel();
    this.money = Util.numberToCurrency(machine.getMoney());
    this.company = new HashMap<>();
    this.company.put("id", machine.getCompany().getId());
    this.company.put("name", machine.getCompany().getName());
  }

  public static List<MachineDTO> machinesToDTOList(List<Machine> machineList) {
    List<MachineDTO> dtoList = new ArrayList<>();

    machineList.forEach(machine -> dtoList.add(new MachineDTO(machine)));

    return dtoList;
  }
}
