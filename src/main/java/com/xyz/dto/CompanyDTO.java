package com.xyz.dto;

import com.xyz.model.Company;
import com.xyz.util.Util;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CompanyDTO {
  private long id;
  private String name;
  private String money;

  public CompanyDTO(Company company) {
    this.id = company.getId();
    this.name = company.getName();
    this.money = Util.numberToCurrency(company.getMoney());
  }

  public static List<CompanyDTO> companiesToDTOList(List<Company> companyList) {
    List<CompanyDTO> dtoList = new ArrayList<>();

    companyList.forEach(company -> dtoList.add(new CompanyDTO(company)));

    return dtoList;
  }
}
