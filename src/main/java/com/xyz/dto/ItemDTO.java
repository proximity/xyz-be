package com.xyz.dto;

import com.xyz.model.Item;
import com.xyz.util.Util;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ItemDTO {
  private long code;
  private String item;
  private String price;

  public ItemDTO(Item item) {
    this.code = item.getCode();
    this.item = item.getItem();
    this.price = Util.numberToCurrency(item.getPrice());
  }

  public static List<ItemDTO> itemsToDTOList(List<Item> itemList) {
    List<ItemDTO> dtoList = new ArrayList<>();

    itemList.forEach(item -> dtoList.add(new ItemDTO(item)));

    return dtoList;
  }
}
