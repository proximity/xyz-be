package com.xyz.service;

import com.xyz.model.Item;
import com.xyz.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
@Repository
public class ItemService extends BaseService<Item, ItemRepository> {
  @Autowired ItemRepository itemRepository;

  @PostConstruct
  public void setVariables() {
    this.setRepository(itemRepository);
  }
}
