package com.xyz.service;

import com.xyz.model.Sale;
import com.xyz.repository.SaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.List;

@Service
@Repository
public class SaleService extends BaseService<Sale, SaleRepository> {
  @Autowired SaleRepository saleRepository;

  @PostConstruct
  public void setVariables() {
    this.setRepository(saleRepository);
  }

  public List<Sale> findSalesByMachineIdStartDateAndEndDate(Long id, Date startDate, Date endDate) {
    return saleRepository.findSalesByMachineIdStartDateAndEndDate(id, startDate, endDate);
  }

  public List<Sale> findSalesByMachineId(Long id) {
    return saleRepository.findSalesByMachineId(id);
  }
}
