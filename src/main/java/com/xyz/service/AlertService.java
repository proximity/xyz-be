package com.xyz.service;

import com.xyz.model.Alert;
import com.xyz.repository.AlertRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
@Repository
public class AlertService extends BaseService<Alert, AlertRepository> {
  @Autowired AlertRepository alertRepository;

  @PostConstruct
  public void setVariables() {
    this.setRepository(alertRepository);
  }

  public Alert findAlertByMachineId(Long id) {
    return alertRepository.findAlertByMachineId(id);
  }
}
