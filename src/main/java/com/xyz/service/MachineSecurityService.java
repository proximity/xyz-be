package com.xyz.service;

import com.xyz.model.MachineSecurity;
import com.xyz.repository.MachineSecurityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
@Repository
public class MachineSecurityService
    extends BaseService<MachineSecurity, MachineSecurityRepository> {
  @Autowired MachineSecurityRepository machineSecurityRepository;

  @PostConstruct
  public void setVariables() {
    this.setRepository(machineSecurityRepository);
  }
}
