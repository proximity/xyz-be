package com.xyz.service;

import com.xyz.model.SoldItem;
import com.xyz.repository.SoldItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
@Repository
public class SoldItemService extends BaseService<SoldItem, SoldItemRepository> {
  @Autowired SoldItemRepository soldItemRepository;

  @PostConstruct
  public void setVariables() {
    this.setRepository(soldItemRepository);
  }

  public List<SoldItem> findSoldItemsBySaleId(Long id) {
    return soldItemRepository.findSoldItemsBySaleId(id);
  }

  public List<SoldItem> findSoldItemsByMachineId(Long id) {
    return soldItemRepository.findSoldItemsByMachineId(id);
  }
}
