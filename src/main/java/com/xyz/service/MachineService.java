package com.xyz.service;

import com.xyz.model.Machine;
import com.xyz.repository.MachineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
@Repository
public class MachineService extends BaseService<Machine, MachineRepository> {
  @Autowired MachineRepository machineRepository;

  @PostConstruct
  public void setVariables() {
    this.setRepository(machineRepository);
  }
}
