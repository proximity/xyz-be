package com.xyz.service;

import com.xyz.model.Company;
import com.xyz.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
@Repository
public class CompanyService extends BaseService<Company, CompanyRepository> {
  @Autowired CompanyRepository companyRepository;

  @PostConstruct
  public void setVariables() {
    this.setRepository(companyRepository);
  }
}
