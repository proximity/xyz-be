package com.xyz;

public final class URLMapping {
  public static final String API = "/api";
  public static final String ID_URL = "/{id}";

  public static final class Company {
    public static final String URL = "/company";
    public static final String BASE_URL = API + URL;
  }

  public static final class Alert {
    public static final String URL = "/alert";
    public static final String BASE_URL = API + URL;
  }

  public static final class Item {
    public static final String URL = "/item";
    public static final String BASE_URL = API + URL;
  }

  public static final class Machine {
    public static final String URL = "/machine";
    public static final String NUMBER_OF_TRANSACTIONS = "/numberOfTransactions";
    public static final String TOTAL_MONEY_EARNED = "/totalMoneyEarned" + ID_URL;
    public static final String SALES = "/sales" + ID_URL;
    public static final String WITHDRAW_MONEY = "/withdrawMoney" + ID_URL;
    public static final String BASE_URL = API + URL;
  }

  public static final class MachineSecurity {
    public static final String URL = "/machineSecurity";
    public static final String OPEN = "/open";
    public static final String UNLOCK = "/unlock";
    public static final String BASE_URL = API + URL;
  }

  public static final class Sale {
    public static final String URL = "/sale";
    public static final String BASE_URL = API + URL;
  }

  public static final class SoldItem {
    public static final String URL = "/soldItem";
    public static final String BASE_URL = API + URL;
  }
}
