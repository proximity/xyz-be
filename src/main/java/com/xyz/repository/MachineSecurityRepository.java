package com.xyz.repository;

import com.xyz.model.MachineSecurity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MachineSecurityRepository extends JpaRepository<MachineSecurity, Long> {}
