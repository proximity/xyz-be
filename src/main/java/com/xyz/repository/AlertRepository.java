package com.xyz.repository;

import com.xyz.model.Alert;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AlertRepository extends JpaRepository<Alert, Long> {
  @Query("SELECT a FROM Alert a WHERE a.machine.id = (:id)")
  Alert findAlertByMachineId(@Param("id") Long id);
}
