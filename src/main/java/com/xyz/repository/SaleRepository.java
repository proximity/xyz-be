package com.xyz.repository;

import com.xyz.model.Sale;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface SaleRepository extends JpaRepository<Sale, Long> {
  @Query(
      "SELECT s FROM Sale s WHERE s.machine.id = (:id) AND s.date >= (:startDate) AND s.date <= (:endDate)")
  List<Sale> findSalesByMachineIdStartDateAndEndDate(
      @Param("id") Long id, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

  @Query("SELECT s FROM Sale s WHERE s.machine.id = (:id)")
  List<Sale> findSalesByMachineId(@Param("id") Long id);
}
