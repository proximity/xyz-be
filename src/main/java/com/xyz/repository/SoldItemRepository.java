package com.xyz.repository;

import com.xyz.model.SoldItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SoldItemRepository extends JpaRepository<SoldItem, Long> {
  @Query("SELECT si FROM SoldItem si WHERE si.sale.id = (:id)")
  List<SoldItem> findSoldItemsBySaleId(@Param("id") Long id);

  @Query("SELECT si FROM SoldItem si WHERE si.sale.machine.id = (:id)")
  List<SoldItem> findSoldItemsByMachineId(@Param("id") Long id);
}
